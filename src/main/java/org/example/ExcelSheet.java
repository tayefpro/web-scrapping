package org.example;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.FileOutputStream;

public class ExcelSheet {
    public  void createExcel() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Invoice");
        String[] headLine = {"Website Scrapping"};
        String[] columHeading = {"Company Serial :", "Company Name : ","Company Type :"};
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.index);
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(headerFont);
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        //
        Row headerRow1 = sheet.createRow(0);
        for(int i=0;i<headLine.length;i++) {
            Cell cell = headerRow1.createCell(i);
            cell.setCellValue(headLine[i]);
            cell.setCellStyle(headerStyle);
        }
          //
        Row headerRow2 = sheet.createRow(2);
        for(int i=0;i<columHeading.length;i++) {
            Cell cell = headerRow2.createCell(i);
            cell.setCellValue(columHeading[i]);
            cell.setCellStyle(headerStyle);
        }
        //
        try {
            int count=0;
            int rowNum=3;
            String url=null;
            for(int i=0; i<=9; i++) {
                url = "https://find-and-update.company-information.service.gov.uk/company/1236575"+count++;
                Document document = Jsoup.connect(url).get();
                Elements serialNumber = document.select("#company-number");
                Elements companyName = document.select(".heading-xlarge");
                Elements companyType = document.select("#company-type");
                System.out.println("Number :" +count);
                System.out.println("Company Serial Number : "+ serialNumber.text());
                System.out.println("Company Name : "+ companyName.text());
                System.out.println("Comapany Type : "+ companyType.text());

                //

                Row row = sheet.createRow(rowNum++);
                    row.createCell(0).setCellValue(serialNumber.text());
                    row.createCell(1).setCellValue(companyName.text());
                    row.createCell(2).setCellValue(companyType.text());


                for (int j = 0; j < columHeading.length; j++) {
                    sheet.autoSizeColumn(j);

                }


                FileOutputStream excelFileOupt = new FileOutputStream("excel.xlsx");
                workbook.write(excelFileOupt);
                if(i>=9){
                    excelFileOupt.close();
                    workbook.close();
                    System.out.println();
                    System.out.println("Excel File Completed.....");
                }
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }
}
